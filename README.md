## Sound Manager Plugin
## Version 1.2
## Created by Celeste Privitera (@Xecestel) & Simón Olivo(@Sarturo)
## Licensed Under MPL 2.0 (see below)

The Sound Manager gives the users a better control over the audio of their games. it is possible to play every sound of the game using just simple method calls. No more long AudioStreamPlayer lists inside your scenes nor long methods to handle the audio inside every script.
It also gives you a better control over the Background Music.  The sounds will not stop between scenes anymore, giving you the power to stop it and play  it whenever and however you want.

This is a plugin designed to facilitate the Sound Manager Module configuration process, by offering an UI in the editor that makes its configuration easier.

This plugin was created for:
- Automatically set the Sound Manager module scene as an autoload.
- Create a dock in the editor to make the configuration easier of the sound manager.

## Configuration

1.  Paste the folder "sound_manager" in the path "res://addons/" from your project (this is the standard path for plugins in Godot).
2.  To activate/deactivate the plugin, go to Project/ Project Settings / Plugins, locate the plugin entry and change its status.
3.  Once the plugin is activated, a dock called "SoundManager" will appear in the editor, there you need to set the paths where the sound files are located, the sound manager handles 4 types of sounds : BGM, BGS, SFX, MFX. (It is not necessary to set all the paths). You can also set the AudioBuses from here too by writing the wanted bus name on the name field ("Master" is the fallback).
4   Save the current scene (presss the shortcut CTRL + S ).

The UI consists of three sections to facilitate the configuration and use of the sound manager, this UI:
- Allows you to set the paths where the sound files are located in your project.
- Allows you to set the audiobuses available in your project.
- Displays the sound files available in each path. 
- Allows you to easily build and edit the Audio FIles Dictionary.

Once set up, you can play/stop the sound files in any scene. To play the sounds you can use the "Audio_Files_Dictionary" ,  it allows you to use different strings (keys) for method calls and for the file names. This way, even if your audio file is called "*sfx_audio_jump.ogg*", you can set it in the dictionary to call it as a simple "Jump", adding a simple entry `"Jump" : "sfx_audio_jump.ogg"`.  The dictionary is located inside the "SoundManager_config.gd" file, but you can use the UI to edit it. You can place it wherever you want inside your project directory.
Another way is to use the names of the sound files, for example: `SoundManager.play_bgm("brackground_music.ogg")`.

## Methods
The main methods of this script are just 12, three for each section of the script (Background Music, Background Sounds, Sound Effects and Music Effecs), plus some useful setters and getters.
**All methods of the Sound Manager are accessed from the singleton called "SoundManager".**

### Main Methods
#### Background Music
The main methods are basically the same but with some differents for background musics (see below), the only difference between them in the method calls is the suffix on the name (`play_bgm` instead of `play_me` for example, or `is_bgm_playing` instead of `is_me_playing`). They can be summarized in three categories:

- `func play(audio : String, reset_to_defaults : bool = true) -> void`: this method lets you play the selected audio, passed as a string. If the audio is already playing, it won't do anything to avoid weird outcomes (or it will replace the previous one for BGMs). The `reset_to_defaults` argument lets you decide if you want the default values for the player property or not. It's useful if you often change volume and pitch (default value: `true`). Note that you can't use `play_bgm` to play a sound effect or music effect and vice versa.

- `func stop(audio : String = "") -> void`: this method lets you stop the stream from playing. The argument (default value: `""`) gives you the ability to tell the stream to stop only if a specific sound is playing. Note that you can't use `stop_bgm` to stop a sound effect or music effect and vice versa.

- `func is_playing(audio : String = "") -> bool`: this method returns `true` if the selected stream is plaing and `false` if not. The argument (default value: `""`) gives you the ability to check if is playing a specific audio file by passing its name. Note that you can't use `is_bgm_playing` to check on a sound effect or music effect and vice versa.

#### Music Effects, Sound Effects and Background Sounds
From the Sound Manager Module version 1.8 you can play multiple musice effects, multiple sound effects and multiple background sounds at once! This means that the methods to work with them are a little bit different in the method calls and require some more words:

- `func play(sound : String, reset_to_defaults : bool = true, override_current_sound : bool = true, sound_to_override : String = "") -> void`: this method is basically the same as the other `play` methods, but comes with some more arguments. `override_current_sound` is an optional boolean variable that allows you to tell the Sound Manager to just replace an already playing sound with a new one. The default value is `true`, but if you go with a `false` the Sound Manager will play the sound without stopping the others, simultaneously. If you go with `true`, however, you can also decide which sound you want to override, by adding the sound name in the optional `sound_to_override` argument. If you don't do that, the Sound Manager automatically overrides the main stream (the one that is already on the scene).

- `stop` and `is_playing` are basically te same as the other sections, they just work differently internally. Note that whenever a Sound Effect or a Background Sound stream player stops (besides the main one), it's deleted from the scene.

- All the getters and setters now require the sound name in order to work. This argument is however always optional: if you don't put any argument in the method call, they'll just assume you are referring to the main AudioStreamPlayer.

- The `get_playing` method now returns an Array of all the currently playing sounds.

### Getters and Setters
- `func set_volume_db(volume_db : float) -> void`: this method allows you to change the selected stream volume via script. `volume_db` is the volume in decibels. (`set_bgm_volume_db` for bgm)

- `func get_volume_db() -> float`: this method return the volume of the given stream. (`get_bgm_volume_db` for bgm)

- `func set_pitch_scale(pitch : float) -> void`: this method allows you to set the pitch scale of the selected stream via script. (`set_bgm_pitch_scale` for bgm)

- `func get_pitch_scale() -> float`: this method returns the pitch scale of the given stream. (`get_bgm_pitch_scale` for bgm)

- `func pause(paused : bool) -> void`: this method allows you to pause or unpause the selected stream. (`pause_bgm` for bgm)

- `func is_paused() -> bool)`: this method returns `true` if the selected stream is paused. (`is_bgm_paused` for bgm)

- `func get_playing() -> String`: this methods (one for each type of audio) return a String contaning the currently playing or last played bgm, bgs, se, or me. (`get_playing_bgm` for bgm)

- `func get_configuration_dictionary() -> Dictionary`: this method returns the configuration dictionary as the user configured it.

- `func get_config_value(stream_name : String) -> String`: this method returns the file name of the given stream name. Returns `null` if an error occured.

- `func set_config_key(new_stream_name : String, new_stream_file : String) -> void`: this method allows the user to edit an existng value on the configuration dictionary, or add a new one in runtime. `new_stream_name` is the name of your choice for the stream (the key in the dictionary), while `new_stream_file` is the name of the file linked to it (the value in the dictionary).


## IMPORTANT NOTES:
For more information about how the Sound Manager Module works, go to https://gitlab.com/Xecestel/sound-manager

## Credits
This plugin was made taking the source code of the Sound Manager project developed by Celeste Privitera (Xecestel).

## Licenses
This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
